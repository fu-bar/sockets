package sockets.udp;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sockets.exceptions.CommunicatorException;
import sockets.structures.NetworkTarget;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import static sockets.structures.Constants.COMMUNICATOR_BUFFER_SIZE;

public class UDPCommunicator implements Runnable {

    private final DatagramSocket transmittingSocket;

    private final Logger log = LogManager.getLogger(UDPCommunicator.class);
    private final Set<Consumer<byte[]>> byteConsumers = new HashSet<>();
    private final int listeningPort;
    private final DatagramSocket socket;
    private boolean running;

    /**
     * UDP Communicator
     *
     * @param listeningPort The port on which the communicator is listening
     */
    public UDPCommunicator(int listeningPort) {
        this.listeningPort = listeningPort;
        try {
            this.socket = new DatagramSocket(this.listeningPort);
            transmittingSocket = new DatagramSocket();
        } catch (SocketException e) {
            throw new CommunicatorException(e.getMessage(), e);
        }
    }

    public UDPCommunicator(int listeningPort, Consumer<byte[]> byteConsumer) {
        this(listeningPort);
        this.byteConsumers.add(byteConsumer);
    }


    /**
     * Send a serializable object as a byte-array to BUS target
     *
     * @param serializable  The object to be serialized and sent
     * @param networkTarget The host and port the byte-array to be sent to
     */
    public void transmit(Serializable serializable, NetworkTarget networkTarget) {
        byte[] bytes = SerializationUtils.serialize(serializable);
        DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length, new InetSocketAddress(networkTarget.getHost(), networkTarget.getPort()));
        try {
            this.transmittingSocket.send(sendPacket);
        } catch (IOException e) {
            throw new CommunicatorException(e.getMessage(), e);
        }
    }


    public void addByteConsumer(Consumer<byte[]> consumer) {
        synchronized (this) {
            this.byteConsumers.add(consumer);
        }
    }

    public void removeByteConsumer(Consumer<byte[]> consumer) {
        synchronized (this) {
            this.byteConsumers.remove(consumer);
        }
    }

    /**
     * Stop listening loop
     */
    public void stopListening() {
        log.debug("Stopping listening loop");
        this.running = false;
    }

    @Override
    public void run() {
        log.debug("Listening for UDP on port {}", listeningPort);
        running = true;

        byte[] buf = new byte[COMMUNICATOR_BUFFER_SIZE];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);

        while (running) {

            try {
                socket.receive(packet);
            } catch (IOException e) {
                throw new CommunicatorException(e.getMessage(), e);
            }

            byte[] receivedBytes = Arrays.copyOfRange(packet.getData(), 0, packet.getLength());
            boolean allBytesZero = true;
            for (byte b : receivedBytes) {
                allBytesZero &= (b == 0);
            }

            if (allBytesZero) {
                log.debug("Received all bytes as zero : stopping");
                running = false;
                continue;
            }
            synchronized (this) {
                for (var byteConsumer : this.byteConsumers) {
                    byteConsumer.accept(receivedBytes);
                }
            }
        }
        socket.close();
    }
}
