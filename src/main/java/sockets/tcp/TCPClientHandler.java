package sockets.tcp;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sockets.exceptions.CommunicatorException;
import sockets.structures.SocketMessage;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class TCPClientHandler extends Thread {
    private final Socket socket;
    private final BlockingQueue<SocketMessage> socketMessages;
    private final String clientHandlerID = "TCPCommunicator_" + this.getName();
    private final boolean isObjectOriented;
    private final InputStream inputStream;
    private final OutputStream outputStream;
    private final ObjectOutputStream objectOutputStream;
    private final ObjectInputStream objectInputStream;
    private final Logger log = LogManager.getLogger(TCPClientHandler.class);
    private boolean isShutdownRequired = false;

    public TCPClientHandler(Socket socket, BlockingQueue<SocketMessage> socketMessages, boolean isObjectOriented) {
        this.socket = socket;
        this.socketMessages = socketMessages;
        this.isObjectOriented = isObjectOriented;
        try {
            this.inputStream = this.socket.getInputStream();
            this.objectInputStream = new ObjectInputStream(inputStream);
            this.outputStream = this.socket.getOutputStream();
            this.objectOutputStream = new ObjectOutputStream(outputStream);
        } catch (IOException e) {
            throw new CommunicatorException(e.getMessage(), e);
        }
    }

    public void transmit(Serializable object) {
        try {
            this.objectOutputStream.writeObject(object);
        } catch (IOException e) {
            log.debug("Transmitting object of type: {}", object.getClass().getName());
            throw new CommunicatorException(e.getMessage(), e);
        }
    }

    public void shutdown() {
        log.debug("Shutting down");
        this.isShutdownRequired = true;
    }

    @Override
    public void run() {
        while (!isShutdownRequired) {
            Serializable object;
            try {
                if (this.isObjectOriented) {
                    object = (Serializable) this.objectInputStream.readObject();
                } else {
                    byte[] bytes = this.inputStream.readAllBytes();
                    object = SerializationUtils.deserialize(bytes);
                }
                log.debug("Received an object of type: {}", object.getClass().getName());
                this.socketMessages.add(new SocketMessage(this.clientHandlerID, object));
            } catch (IOException | ClassNotFoundException e) {
                throw new CommunicatorException(e.getMessage(), e);
            }
        }
    }

    public String getClientHandlerID() {
        return clientHandlerID;
    }
}
