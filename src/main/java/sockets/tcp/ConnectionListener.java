package sockets.tcp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sockets.exceptions.CommunicatorException;
import sockets.structures.SocketMessage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

public class ConnectionListener extends Thread {
    private final int connectionPort;
    private final Logger log;
    private final BlockingQueue<SocketMessage> socketMessages;
    private final ServerSocket serverSocket;
    private final Set<TCPClientHandler> clientHandlers = new HashSet<>();
    private final boolean isObjectOriented;
    private boolean isShutdownRequired = false;

    public ConnectionListener(int connectionPort, BlockingQueue<SocketMessage> socketMessages, boolean isObjectOriented) {
        this.connectionPort = connectionPort;
        this.socketMessages = socketMessages;
        this.isObjectOriented = isObjectOriented;
        try {
            serverSocket = new ServerSocket(connectionPort);
        } catch (IOException e) {
            throw new CommunicatorException(e.getMessage(), e);
        }
        this.log = LogManager.getLogger("ConnectionListener_" + this.getName());
    }

    public void shutdown() {
        this.isShutdownRequired = true;
    }

    @Override
    public void run() {
        this.log.info("Starting listening for TCP connections on port {}", connectionPort);
        while (!isShutdownRequired) {
            try {
                Socket clientSocket = serverSocket.accept();
                TCPClientHandler clientHandler = new TCPClientHandler(clientSocket, socketMessages, isObjectOriented);
                clientHandler.start();
                clientHandlers.add(clientHandler);
                this.log.debug("Accepted new connection from a client: {}", clientSocket.getRemoteSocketAddress());
            } catch (IOException e) {
                throw new CommunicatorException(e.getMessage(), e);
            }
        }

    }
}
