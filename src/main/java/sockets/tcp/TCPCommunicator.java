package sockets.tcp;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sockets.exceptions.CommunicatorException;
import sockets.structures.NetworkTarget;
import sockets.structures.SocketMessage;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

public class TCPCommunicator implements Consumer<SocketMessage> {

    private final Set<TCPClientHandler> clientHandlers = new HashSet<>();
    private final BlockingQueue<SocketMessage> socketMessages = new LinkedBlockingQueue<>(1000);
    private final Set<Consumer<SocketMessage>> messageConsumers = new HashSet<>();
    private final Logger log;
    private ConnectionListener connectionListener;
    private MessageDispatcher messageDispatcher;
    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private OutputStream outputStream;
    private boolean isObjectOriented;

    public TCPCommunicator() {
        this.isObjectOriented = false;
        this.log = LogManager.getLogger(TCPCommunicator.class);
    }

    public TCPCommunicator(boolean isObjectOriented) {
        this();
        this.isObjectOriented = isObjectOriented;
    }


    public void connect(NetworkTarget networkTarget) {
        try {
            this.socket = new Socket(networkTarget.getHost(), networkTarget.getPort());
            this.outputStream = socket.getOutputStream();
            this.objectOutputStream = new ObjectOutputStream(this.outputStream);
            log.debug("Client connected to {}:{}", networkTarget.getHost(), networkTarget.getPort());
        } catch (IOException e) {
            throw new CommunicatorException(e.getMessage(), e);
        }
    }

    public void transmit(Serializable object) {
        try {
            if (this.isObjectOriented) {
                this.objectOutputStream.writeObject(object);
            } else {
                byte[] bytes = SerializationUtils.serialize(object);
                this.outputStream.write(bytes);
            }
        } catch (IOException e) {
            throw new CommunicatorException(e.getMessage(), e);
        }
    }

    public void addMessageConsumer(Consumer<SocketMessage> consumer) {
        synchronized (this) {
            this.messageConsumers.add(consumer);
        }
    }

    public void removeMessageConsumer(Consumer<SocketMessage> consumer) {
        synchronized (this) {
            this.messageConsumers.remove(consumer);
        }
    }

    public void transmit(SocketMessage socketMessage) {
        Optional<TCPClientHandler> handler = this.clientHandlers.stream()
                .filter(clientHandler -> clientHandler.getClientHandlerID().equals(socketMessage.getSenderId()))
                .findAny();
        if (handler.isEmpty()) {
            throw new CommunicatorException(String.format("Failed locating handler with id: %s", socketMessage.getSenderId()));
        } else {
            TCPClientHandler clientHandler = handler.get();
            clientHandler.transmit(socketMessage.getObject());
        }
    }

    public void startServer(int connectionPort) {
        this.connectionListener = new ConnectionListener(connectionPort, socketMessages, isObjectOriented);
        this.connectionListener.start();
        this.messageDispatcher = new MessageDispatcher(messageConsumers, socketMessages, this);
        this.messageDispatcher.start();
    }

    @Override
    public void accept(SocketMessage socketMessage) {
        synchronized (this) {
            try {
                this.messageConsumers.forEach(consumer -> consumer.accept(socketMessage));
            } catch (Throwable e) {
                String errorMessage = String.format("Message delivery resulted with an exception: %s", e.getMessage());
                throw new CommunicatorException(errorMessage, e);
            }
        }
    }


}
