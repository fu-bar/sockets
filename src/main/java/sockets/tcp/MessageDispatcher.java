package sockets.tcp;

import sockets.exceptions.CommunicatorException;
import sockets.structures.SocketMessage;

import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.function.Consumer;

public class MessageDispatcher extends Thread {

    private final BlockingQueue<SocketMessage> socketMessages;
    private final Set<Consumer<SocketMessage>> messageConsumers;
    private final Object lock;
    private boolean isShutdownRequired = false;


    public MessageDispatcher(Set<Consumer<SocketMessage>> messageConsumers, BlockingQueue<SocketMessage> socketMessages, Object lock) {
        this.messageConsumers = messageConsumers;
        this.socketMessages = socketMessages;
        this.lock = lock;
    }

    public void shutdown() {
        this.isShutdownRequired = true;
    }

    @Override
    public void run() {
        while (!isShutdownRequired) {
            try {
                SocketMessage socketMessage = socketMessages.take();
                synchronized (lock) {
                    messageConsumers.forEach(consumer -> consumer.accept(socketMessage));
                }
            } catch (InterruptedException e) {
                this.interrupt();
                throw new CommunicatorException(e.getMessage(), e);
            }
        }
    }
}
