package sockets.exceptions;

public class CommunicatorException extends RuntimeException {
    public CommunicatorException(String errorMessage) {
        super(errorMessage);
    }

    public CommunicatorException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
