package sockets.structures;

import java.io.Serializable;

public class SocketMessage implements Serializable {
    private final String senderId;
    private final Object object;

    public SocketMessage(String senderId, Serializable object) {
        this.senderId = senderId;
        this.object = object;
    }

    public String getSenderId() {
        return senderId;
    }

    public Serializable getObject() {
        return (Serializable) object;
    }
}
