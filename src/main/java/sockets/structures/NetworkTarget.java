package sockets.structures;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class NetworkTarget {
    private final String host;
    private final int port;

    public NetworkTarget(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        NetworkTarget networkTarget = (NetworkTarget) o;

        return new EqualsBuilder().append(port, networkTarget.port).append(host, networkTarget.host).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(host).append(port).toHashCode();
    }
}
